//
//  SelectNumberController.h
//  TuneTalk
//
//  Created by AshtonTam on 17/01/2018.
//  Copyright © 2018 Nexstream. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"
#import "LocalizedString.h"
#import "Constants.h"
#import "UIViewController+Other.h"
#import <SkyFloatingLabelTextField/SkyFloatingLabelTextField-Swift.h>
#import "RegistrationDetail.h"

@interface SelectNumberController : UIViewController

@property NSString* selectedNumber;
@property NSString* apiKey;
@property NSString* registerId;
@property NSString* simNumber;
@property (nonatomic, copy, nullable) void (^completion)(NSInteger, NSString*, RegistrationDetail*);

@end
