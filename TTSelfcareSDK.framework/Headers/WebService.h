//
//  WebService.h
//  TuneTalk
//
//  Created by Shah Jahan on 12/26/14.
//  Copyright (c) 2014 Nexstream. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import <MMProgressHUD/MMProgressHUD.h>
#import "WebService.h"
#import "LocalizedString.h"
#import "Constants.h"
#import "NSDictionary+Other.h"
#import "SelfRegistration.h"
#import "RegistrationDetail.h"

@protocol WebServiceDelegate;
@interface WebService : NSObject

@property NSTimer* timer;
@property (nonatomic) id <WebServiceDelegate> delegate;

-(void)sdkInit:(NSString*)bundleID apiKey:(NSString*)apiKey platform:(NSString*)platform;
-(void)registerStep1:(NSString*)apiKey;
-(void)registerStep2:(NSString*)apiKey msisdn:(nullable NSString*)msisdn registerId:(NSString*)registerId simNumber:(NSString*)simNumber;
-(void)checkSimType:(NSString*)apiKey simNum:(NSString*)simNum;
-(void)getNumberFrom4Digits:(NSString*)apiKey digits:(NSString*)digits;
@end


@protocol WebServiceDelegate <NSObject>

@optional
-(void)sdkInitResponse:(BOOL)isSuccessful message:(nullable NSString*)message microBlinkLicenseKey:(nullable NSString*)microBlinkLicenseKey;
-(void)registerStep1Response:(NSInteger)resultCode message:(nullable NSString*)message registerId:(nullable NSString*)registerId;
-(void)registerStep2Response:(NSInteger)resultCode message:(nullable NSString*)message regDetail:(nullable RegistrationDetail*)regDetail;
-(void)getNumberFrom4DigitsResponse:(NSArray *)numberList;
-(void)checkSimTypeResponse:(BOOL)isValidSim simNumber:(nullable NSString*)simNumber;

@end

