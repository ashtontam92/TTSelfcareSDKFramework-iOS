//
//  UIViewController+Actionsheet.h
//  TuneTalk
//
//  Created by AshtonTam on 16/01/2018.
//  Copyright © 2018 Nexstream. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocalizedString.h"
#import "Constants.h"

@interface UIViewController (Other)

-(void)showActionSheet:(NSArray*)dataArr sourceView:(UIView*)sourceView completion:(void (^)(NSString*))completion;

-(void)showAlert:(NSString*)title message:(NSString*)message dataArr:(NSArray*)dataArr completion:(void (^)(NSString*))completion;

-(void)showAlertWithSelection:(NSString*)title message:(NSString*)message yesTitle:(NSString*)yesTitle yesCompletion:(void (^)(void))yesCompletion noTitle:(NSString*)noTitle noCompletion:(void (^)(void))noCompletion;

-(void)showAlertWithOption:(NSString*)title

                   message:(NSString*)message options:(NSArray*)options completion:(void (^)(NSString*))completion;

-(__kindof UIViewController*)initControllerWith:(NSString*)named identifier:(NSString*)storyboardID;

-(UIViewController*)topViewController;

-(NSString*)toDateFromMilliSecond:(NSString*)millisecond withFormat:(NSString*)format;

-(void)pushControllerWith:(NSString*)storyboardID identifier:(NSString*)identifier completion:(void (^)(UIViewController*))completion;

-(void)presentControllerWith:(NSString*)storyboardID identifier:(NSString*)identifier animated:(BOOL)animated completion:(void (^)(UIViewController*))completion;

-(UIView*)createFooterBtnView:(UITableView*)tableView title:(NSString*)title font:(UIFont*)font backgroundColor:(UIColor*)backgroundColor textColor:(UIColor*)textColor selector:(SEL)selector margin:(CGFloat)margin;
@end
