//
//  LocalizedString.h
//  TuneTalk
//
//  Created by Rex Ong on 11/08/2016.
//  Copyright © 2016 Nexstream. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocalizedString : NSObject
+(NSBundle*) bundle;
+(void)setLanguage:(NSString *)l;
+(NSString*)getLocalizedStringFor :(NSString *)key alter:(NSString *)alternate;
+(NSString*)getLocalizedStringFor :(NSString *)key;
@end
